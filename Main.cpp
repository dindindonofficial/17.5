#include <iostream>
#include <cmath>

class Vector
{
public:

	Vector() : x(0), y(0), z(0)
	{}
	Vector(float _x, float _y, float _z) : x(_x), y(_y), z(_z)
	{}
	void show() 
	{
		std::cout << "Parameters:" << ' ' << x << ' ' << y << ' ' << z << '\n' << "Vector length:" << ' ' << std::sqrt(x * x + y * y + z * z);

	}

private:
	float x;
	float y;
	float z;
};

int main()
{
	Vector v(10, 10, 10);
	v.show();
}